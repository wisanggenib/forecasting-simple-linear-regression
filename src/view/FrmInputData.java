/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package view;

import controller.AdminController;
import controller.HasilController;
import controller.PenjualanController;
import java.sql.Date;
import java.text.SimpleDateFormat;
import java.util.List;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;
import model.PenjualanModel;

/**
 *
 * @author Abo
 */
public class FrmInputData extends javax.swing.JFrame {

    private PenjualanController control;
    private HasilController control1;
    private boolean status;
    private String proses;
    private List<PenjualanModel> listPenjualan;
    private String[] tabelHeader;
    private DefaultTableModel dtm;
    private int row;
    private int idPeriodeSementara;
//    int total = 0;
//    int idPeriod = 1;
//    int jumlahPenjualan = 0;
//    int jumlahPeriode = 0;
//    int kuadrat = 0;
//    int jumpenju = 0;
//    int sementara = 0;
//    float b = 0;

    /**
     * Creates new form FrmInputData
     */
    public FrmInputData() {
        initComponents();
        control = new PenjualanController(this);
        control1 = new HasilController(this);

        proses = "";
        tabelHeader = new String[]{"Id Periode", "Jumlah Penjualan"};
        dtm = new DefaultTableModel(null, tabelHeader);
        tblData.setModel(dtm);
        tblData.getSelectionModel().addListSelectionListener(new ListSelectionListener() {

            public void valueChanged(ListSelectionEvent e) {
                //throw new UnsupportedOperationException("Not supported yet.");
                row = tblData.getSelectedRow();
                if (row >= 0) {
//                    txtNik.setText(dtm.getValueAt(row, 0).toString());
//                    txtNama.setText(dtm.getValueAt(row, 1).toString());
//                    txtTelp.setText(dtm.getValueAt(row, 2).toString());
//                    txtPassword.setText(dtm.getValueAt(row, 3).toString());
                }
            }
        });
        TampilData();
    }

    public void TampilData() {
        if (proses.equalsIgnoreCase("")) {
            listPenjualan = control.tampilPenjualan();
        }

        dtm = (DefaultTableModel) tblData.getModel();
        dtm.setRowCount(0);
        for (PenjualanModel data : listPenjualan) {
            dtm.addRow(new Object[]{
                data.getPeriode(),
                data.getJumlah()});
        }

        if (tblData.getRowCount() > 0) {
            row = tblData.getRowCount() - 1;
            tblData.setRowSelectionInterval(row, row);
//            aturButton(true);
//            aturText(true);
            proses = "";
        }

        idPeriodeSementara = tblData.getRowCount();
        txtIdPeriode.setText(String.valueOf(idPeriodeSementara + 1));
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jScrollBar1 = new javax.swing.JScrollBar();
        panel_input = new javax.swing.JPanel();
        lbl_input_data = new javax.swing.JLabel();
        lbl_catatan = new javax.swing.JLabel();
        btn_proses = new javax.swing.JButton();
        btn_cleartbl = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        tblData = new javax.swing.JTable();
        btn_input = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        logo = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtIdPeriode = new javax.swing.JTextField();
        jLabel5 = new javax.swing.JLabel();
        txtJumlah = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        panel_input.setBackground(new java.awt.Color(255, 102, 102));

        lbl_input_data.setFont(new java.awt.Font("Source Sans Pro", 1, 18)); // NOI18N
        lbl_input_data.setText("INPUT DATA");

        lbl_catatan.setFont(new java.awt.Font("Segoe UI Semibold", 1, 12)); // NOI18N
        lbl_catatan.setText("*Catatan : 1. Isikan kolom \"Jumlah Penjualan\" lalu ");

        btn_proses.setText("PROSES");
        btn_proses.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_prosesActionPerformed(evt);
            }
        });

        btn_cleartbl.setText("KOSONGKAN TABEL");
        btn_cleartbl.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cleartblActionPerformed(evt);
            }
        });

        tblData.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Id Periode", "Periode Penjualan", "Jumlah Penjualan"
            }
        ));
        jScrollPane1.setViewportView(tblData);

        btn_input.setText("INPUT");
        btn_input.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_inputActionPerformed(evt);
            }
        });

        jLabel1.setText("klik tombol \"INPUT\"");

        jLabel2.setText("2. Setelah selesai input data, selanjutnya");

        jLabel3.setText("klik tombol \"PROSES\"");

        logo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/abo.png"))); // NOI18N

        jLabel4.setText("Id Periode :");

        txtIdPeriode.setEditable(false);

        jLabel5.setText("Jumlah Penjualan :");

        javax.swing.GroupLayout panel_inputLayout = new javax.swing.GroupLayout(panel_input);
        panel_input.setLayout(panel_inputLayout);
        panel_inputLayout.setHorizontalGroup(
            panel_inputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_inputLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(logo)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(lbl_input_data)
                .addGap(200, 200, 200))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_inputLayout.createSequentialGroup()
                .addGap(79, 79, 79)
                .addComponent(btn_proses, javax.swing.GroupLayout.PREFERRED_SIZE, 132, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(btn_cleartbl)
                .addGap(83, 83, 83))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_inputLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(panel_inputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_inputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_inputLayout.createSequentialGroup()
                            .addComponent(lbl_catatan)
                            .addGap(117, 117, 117))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_inputLayout.createSequentialGroup()
                            .addComponent(btn_input, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addContainerGap())
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_inputLayout.createSequentialGroup()
                            .addComponent(jLabel2)
                            .addGap(131, 131, 131)))
                    .addGroup(panel_inputLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(jLabel3)
                        .addGap(43, 43, 43))))
            .addGroup(panel_inputLayout.createSequentialGroup()
                .addGroup(panel_inputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_inputLayout.createSequentialGroup()
                        .addGap(57, 57, 57)
                        .addComponent(jLabel4)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtIdPeriode, javax.swing.GroupLayout.PREFERRED_SIZE, 103, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtJumlah, javax.swing.GroupLayout.PREFERRED_SIZE, 102, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(panel_inputLayout.createSequentialGroup()
                        .addGap(210, 210, 210)
                        .addComponent(jLabel1)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(panel_inputLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 506, Short.MAX_VALUE)
                .addContainerGap())
        );
        panel_inputLayout.setVerticalGroup(
            panel_inputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(panel_inputLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(panel_inputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(panel_inputLayout.createSequentialGroup()
                        .addComponent(logo)
                        .addGap(7, 7, 7))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, panel_inputLayout.createSequentialGroup()
                        .addComponent(lbl_input_data)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)))
                .addComponent(lbl_catatan)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel1)
                .addGap(11, 11, 11)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 24, Short.MAX_VALUE)
                .addGroup(panel_inputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel4)
                    .addComponent(txtIdPeriode, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5)
                    .addComponent(txtJumlah, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btn_input, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(11, 11, 11)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 220, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(panel_inputLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(btn_proses)
                    .addComponent(btn_cleartbl))
                .addGap(19, 19, 19))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panel_input, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panel_input, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_inputActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_inputActionPerformed
        // TODO add your handling code here:
        control.inputTransaksi(txtJumlah.getText(), txtIdPeriode.getText());
        boolean hasil = control.insertTrasaksi();
        if (hasil == false) {
            JOptionPane.showMessageDialog(null, "Penyimpanan gagal");
        } else {
            TampilData();
        }
    }//GEN-LAST:event_btn_inputActionPerformed

    private void btn_cleartblActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cleartblActionPerformed
        // TODO add your handling code here:
        control.deleteData();
        boolean hasil = control.deleteData();
        if (hasil == false) {
            JOptionPane.showMessageDialog(null, "hapus gagal");
        } else {
            TampilData();
        }
        
    }//GEN-LAST:event_btn_cleartblActionPerformed

    private void btn_prosesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_prosesActionPerformed
        // TODO add your handling code here:

        
        int i;
        int jumlahY = 0;
        int jumlahX2 = 0;
        int jumlahXkaliY = 0;
        int jumlahX = 0;
        int jumlahXdikuadrat = 0;

        // cara dapet value dari table
        //int id =  Integer.parseInt(String.valueOf(tblData.getModel().getValueAt(1, 0)));
        for (i = 1; i <= idPeriodeSementara; i++) {
            jumlahX = jumlahX + i;
        }
        for (i = 0; i < idPeriodeSementara; i++) {
            jumlahY = jumlahY + Integer.parseInt(String.valueOf(tblData.getModel().getValueAt(i, 1)));
            jumlahX2 = jumlahX2 + (Integer.parseInt(String.valueOf(tblData.getModel().getValueAt(i, 0))) * Integer.parseInt(String.valueOf(tblData.getModel().getValueAt(i, 0))));
            jumlahXkaliY = jumlahXkaliY + (Integer.parseInt(String.valueOf(tblData.getModel().getValueAt(i, 0))) * Integer.parseInt(String.valueOf(tblData.getModel().getValueAt(i, 1))));
        }
        jumlahXdikuadrat = jumlahX * jumlahX;
        float b1, b2, b, a, y;
        b1 = ((idPeriodeSementara * jumlahXkaliY) - (jumlahX * jumlahY));
        b2 = ((idPeriodeSementara * jumlahX2) - jumlahXdikuadrat);
        b = b1 / b2;

        a = (jumlahY - (b * jumlahX)) / idPeriodeSementara;
        y = a + (b * idPeriodeSementara);

        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        String tglskrg = formatter.format(date);
        
        control1.inputHasil(String.valueOf(idPeriodeSementara + 1), tglskrg, String.valueOf(y));
        boolean hasil = control1.insertHasil();
        if (hasil == false) {
            JOptionPane.showMessageDialog(null, "Penyimpanan gagal");
        } else {
            FrmHasilForecasting form = new FrmHasilForecasting();
            form.setVisible(true);
            this.dispose();
        }
    }//GEN-LAST:event_btn_prosesActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrmInputData.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrmInputData.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrmInputData.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrmInputData.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrmInputData().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btn_cleartbl;
    private javax.swing.JButton btn_input;
    private javax.swing.JButton btn_proses;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JScrollBar jScrollBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JLabel lbl_catatan;
    private javax.swing.JLabel lbl_input_data;
    private javax.swing.JLabel logo;
    private javax.swing.JPanel panel_input;
    private javax.swing.JTable tblData;
    private javax.swing.JTextField txtIdPeriode;
    private javax.swing.JTextField txtJumlah;
    // End of variables declaration//GEN-END:variables

    private void insertPenjualan() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}

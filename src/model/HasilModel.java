/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import koneksi.KoneksiDB;

/**
 *
 * @author Abo
 */
public class HasilModel {

    private KoneksiDB koneksi;
    private String query;
    private ResultSet rsHasil;
    private boolean status;
    private List<HasilModel> listHasil;

    public HasilModel() {
        koneksi = new KoneksiDB();
        koneksi.getConn();
    }

    private String periode;
    private String tanggal;
    private String nilai;

    public String getPeriode() {
        return periode;
    }

    public void setPeriode(String periode) {
        this.periode = periode;
    }

    public String getTanggal() {
        return tanggal;
    }

    public void setTanggal(String tanggal) {
        this.tanggal = tanggal;
    }

    public String getNilai() {
        return nilai;
    }

    public void setNilai(String nilai) {
        this.nilai = nilai;
    }
    
    //variable statis menyimpan data sementara
    private static String periodeSementara;
    private static String tanggalSementara;
    private static String nilaiSementara;

    public static String getPeriodeSementara() {
        return periodeSementara;
    }

    public static void setPeriodeSementara(String periodeSementara) {
        HasilModel.periodeSementara = periodeSementara;
    }

    public static String getTanggalSementara() {
        return tanggalSementara;
    }

    public static void setTanggalSementara(String tanggalSementara) {
        HasilModel.tanggalSementara = tanggalSementara;
    }

    public static String getNilaiSementara() {
        return nilaiSementara;
    }

    public static void setNilaiSementara(String nilaiSementara) {
        HasilModel.nilaiSementara = nilaiSementara;
    }
    
    

    public boolean insertHasil() {
        query = "INSERT INTO hasil (periode,tanggal_input,nilai_hasil) VALUES ('" + periode + "','" + tanggal + "','" + nilai + "')";

        status = koneksi.eksekusiQuery(query, false);
        return status;
    }
    
        public boolean tampilHasil() {
        query = "SELECT periode,tanggal_input,nilai_hasil FROM hasil ORDER BY id_hasil DESC LIMIT 1";
        status = koneksi.eksekusiQuery(query, true);
        if(status){
            rsHasil = koneksi.getRs();
            try {
                rsHasil.next();
                periodeSementara = rsHasil.getString(1);
                tanggalSementara = rsHasil.getString(2);
                nilaiSementara = rsHasil.getString(3);
                if(periodeSementara == null){
                    status = false;
                }else{
                    status = true;
                }
            } catch (SQLException se) {
                status = false;
            }
        }
        return status;
    }
}

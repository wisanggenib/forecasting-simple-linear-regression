/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import model.HasilModel;
import model.PenjualanModel;
import view.FrmHasilForecasting;
import view.FrmInputData;

/**
 *
 * @author Abo
 */
public class HasilController {

    private HasilModel model;
    private FrmInputData view;
    private FrmHasilForecasting view2;

    public HasilController(FrmInputData view) {
        this.view = view;
        model = new HasilModel();
    }
    public HasilController(FrmHasilForecasting view2) {
        this.view2 = view2;
        model = new HasilModel();
    }

    public void inputHasil(String periode, String tanggal,String nilai) {
        model.setPeriode(periode);
        model.setTanggal(tanggal);
        model.setNilai(nilai);
    }

    public boolean insertHasil() {
        return model.insertHasil();
    }
    public boolean tampilHasil() {
        return model.tampilHasil();
    }
    public static String getPeriodeSementara() {
        return HasilModel.getPeriodeSementara();
    }
    public static String getTanggalSementara() {
        return HasilModel.getTanggalSementara();
    }
    public static String getNilaiSementara() {
        return HasilModel.getNilaiSementara();
    }

}

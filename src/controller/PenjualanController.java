/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import java.util.List;
import model.PenjualanModel;
import view.FrmInputData;

/**
 *
 * @author Abo
 */
public class PenjualanController {

    private PenjualanModel model;
    private FrmInputData view;

    public PenjualanController(FrmInputData view) {
        this.view = view;
        model = new PenjualanModel();
    }

    public void inputTransaksi(String jumlah, String periode) {
        model.setJumlah(jumlah);
        model.setPeriode(periode);
    }

    public boolean insertTrasaksi() {
        return model.insertTransaksi();
    }
    
    public boolean deleteData() {
        return model.deteleData();
    }

    public List tampilPenjualan() {
        return model.tampilPenjualan();
    }
}
